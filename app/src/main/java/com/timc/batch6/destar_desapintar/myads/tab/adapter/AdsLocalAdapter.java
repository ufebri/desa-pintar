package com.timc.batch6.destar_desapintar.myads.tab.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.model.AdsLocal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/19/18.
 */

public class AdsLocalAdapter extends RecyclerView.Adapter<AdsLocalAdapter.IklanViewHolder> {

    private List<AdsLocal> adsLocalList = new ArrayList<>();

    public AdsLocalAdapter(List<AdsLocal> adsLocalList) {
        this.adsLocalList = adsLocalList;
    }


    @Override
    public AdsLocalAdapter.IklanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads, parent, false);
        IklanViewHolder iklanViewHolder = new IklanViewHolder(view);
        return iklanViewHolder;
    }

    @Override
    public void onBindViewHolder(AdsLocalAdapter.IklanViewHolder holder, int position) {

        holder.Juduliklan.setText(adsLocalList.get(position).getJudulIklan());
        holder.Hargaiklan.setText(adsLocalList.get(position).getHargaIklan());

    }

    @Override
    public int getItemCount() {
        return adsLocalList.size();
    }

    public class IklanViewHolder extends RecyclerView.ViewHolder {

        TextView Juduliklan;
        TextView Hargaiklan;

        public IklanViewHolder(View itemView) {
            super(itemView);

            Juduliklan = itemView.findViewById(R.id.tv_nama_iklan);
            Hargaiklan = itemView.findViewById(R.id.tv_harga);

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
