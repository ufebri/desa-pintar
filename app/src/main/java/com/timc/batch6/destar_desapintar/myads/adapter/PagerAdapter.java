package com.timc.batch6.destar_desapintar.myads.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.timc.batch6.destar_desapintar.myads.tab.ActiveAdsFragment;
import com.timc.batch6.destar_desapintar.myads.tab.ModerateAdsFragment;
import com.timc.batch6.destar_desapintar.myads.tab.NonActiveAdsFragment;
import com.timc.batch6.destar_desapintar.myads.tab.RejectAdsFragment;

/**
 * Created by user on 4/4/18.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int number_tabs;

    public PagerAdapter(FragmentManager fm, int number_tabs) {
        super(fm);
        this.number_tabs = number_tabs;
    }

    //Mengembalikan Fragment yang terkait dengan posisi tertentu
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ActiveAdsFragment();
            case 1:
                return new ModerateAdsFragment();
            case 2:
                return new NonActiveAdsFragment();
            case 3:
                return new RejectAdsFragment();
            default:
                return null;
        }
    }

    //Mengembalikan jumlah tampilan yang tersedia.
    @Override
    public int getCount() {
        return number_tabs;
    }

    public  CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Aktif";
            case 1:
                return "Moderasi";
            case 2:
                return "Non-Aktif";
            case 3:
                return "Ditolak";
        }
        return null;
    }

}
