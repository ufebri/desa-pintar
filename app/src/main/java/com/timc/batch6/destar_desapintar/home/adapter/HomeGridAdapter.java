package com.timc.batch6.destar_desapintar.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.content.DetailContentActivity;
import com.timc.batch6.destar_desapintar.content.slider.SliderView;
import com.timc.batch6.destar_desapintar.home.GridHomeFragment;
import com.timc.batch6.destar_desapintar.home.model.Item;
import com.timc.batch6.destar_desapintar.home.model.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class HomeGridAdapter extends RecyclerView.Adapter<HomeGridAdapter.ViewHolder> {

    private ArrayList<Integer> gambarProduk = new ArrayList<>();
    private ArrayList<String> namaProduk = new ArrayList<>();
    private ArrayList<String> hargaProduk = new ArrayList<>();
    private Context mContext;


    public HomeGridAdapter(Context context,
                           ArrayList<Integer> imageProduct,
                           ArrayList<String> nameProduct,
                           ArrayList<String> priceProduct) {
        gambarProduk = imageProduct;
        namaProduk = nameProduct;
        hargaProduk = priceProduct;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_home,
                parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.gambar.setImageResource(gambarProduk.get(position));
        holder.nama.setText(namaProduk.get(position));
        holder.harga.setText(hargaProduk.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailContentActivity.class);
                intent.putExtra("nama_Produk", namaProduk.get(position));
                intent.putExtra("harga_Produk", hargaProduk.get(position));
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return namaProduk.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView gambar;
        TextView nama;
        TextView harga;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

        gambar = itemView.findViewById(R.id.iv_grid);
        nama = itemView.findViewById(R.id.tv_title);
        harga = itemView.findViewById(R.id.tv_price);
        parentLayout = itemView.findViewById(R.id.llItemGrid);

        }
    }
}
