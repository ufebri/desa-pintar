package com.timc.batch6.destar_desapintar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/21/18.
 */

public class NotifBarter {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("picture")
    @Expose
    private Object picture;
    @SerializedName("user_product")
    @Expose
    private Integer userProduct;
    @SerializedName("user_product_barter")
    @Expose
    private String userProductBarter;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getPicture() {
        return picture;
    }

    public void setPicture(Object picture) {
        this.picture = picture;
    }

    public Integer getUserProduct() {
        return userProduct;
    }

    public void setUserProduct(Integer userProduct) {
        this.userProduct = userProduct;
    }

    public String getUserProductBarter() {
        return userProductBarter;
    }

    public void setUserProductBarter(String userProductBarter) {
        this.userProductBarter = userProductBarter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
