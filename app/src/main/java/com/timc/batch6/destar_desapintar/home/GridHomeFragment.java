package com.timc.batch6.destar_desapintar.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.content.DetailContentActivity;
import com.timc.batch6.destar_desapintar.home.adapter.HomeGridAdapter;
import com.timc.batch6.destar_desapintar.home.model.Item;
import com.timc.batch6.destar_desapintar.home.model.OnItemClickListener;
import com.timc.batch6.destar_desapintar.model.Ads;
import com.timc.batch6.destar_desapintar.myads.network.BaseApp;
import com.timc.batch6.destar_desapintar.myads.tab.adapter.ActiveAdsAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GridHomeFragment extends Fragment {

    private ArrayList<Integer> gambarProduk = new ArrayList<>();
    private ArrayList<String> namaProduk = new ArrayList<>();
    private ArrayList<String> hargaProduk = new ArrayList<>();

    public GridHomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid_home, container, false);


        getItem(view);

        return view;

    }

    private void getItem(View view) {
        gambarProduk.add(R.drawable.telur_asin);
        namaProduk.add("Telur Asin");
        hargaProduk.add("Rp. 15.000");

        gambarProduk.add(R.drawable.sayuran);
        namaProduk.add("Sayuran Segar");
        hargaProduk.add("Rp. 5.000");

        gambarProduk.add(R.drawable.emping);
        namaProduk.add("Emping Renyah");
        hargaProduk.add("Rp. 7.000");

        gambarProduk.add(R.drawable.gerabah);
        namaProduk.add("Gerabah Berkualitas");
        hargaProduk.add("Rp. 70.000");

        gambarProduk.add(R.drawable.cabe_merah);
        namaProduk.add("Cabe Merah Segar");
        hargaProduk.add("Rp. 10.000");

        gambarProduk.add(R.drawable.tomat);
        namaProduk.add("Tomat Segar");
        hargaProduk.add("Rp. 8000");

        gambarProduk.add(R.drawable.jahitan);
        namaProduk.add("Jahit Murah");
        hargaProduk.add("Rp. 180.000");

        gambarProduk.add(R.drawable.telor_asin);
        namaProduk.add("Mie Kuah Sedap");
        hargaProduk.add("Rp. 10.000");

        gambarProduk.add(R.drawable.sayuran);
        namaProduk.add("Sayuran Segar");
        hargaProduk.add("Rp. 5.000");

        gambarProduk.add(R.drawable.emping);
        namaProduk.add("Emping Renyah");
        hargaProduk.add("Rp. 7.000");

        gambarProduk.add(R.drawable.gerabah);
        namaProduk.add("Gerabah Berkualitas");
        hargaProduk.add("Rp. 70.000");

        gambarProduk.add(R.drawable.cabe_merah);
        namaProduk.add("Cabe Merah Segar");
        hargaProduk.add("Rp. 10.000");

        gambarProduk.add(R.drawable.tomat);
        namaProduk.add("Tomat Segar");
        hargaProduk.add("Rp. 8000");

        gambarProduk.add(R.drawable.kambing);
        namaProduk.add("Kambing");
        hargaProduk.add("Rp. 5.000.000");

        initRecyclerView(view);

    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recGridHome);
        HomeGridAdapter adapter = new HomeGridAdapter(getContext(), gambarProduk, namaProduk, hargaProduk);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

    }
}
