package com.timc.batch6.destar_desapintar.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.MainActivity;

/**
 * Created by user on 3/26/18.
 */

public class LoginPage extends AppCompatActivity {

    EditText username, password;
    Button login;

    String userid, passid;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       getSupportActionBar().hide();

        username = findViewById(R.id.edt_username);
        password = findViewById(R.id.edt_password);
        login = findViewById(R.id.btn_login);

        final Intent intent = new Intent(this, MainActivity.class);

        userid = username.getText().toString();
        passid = password.getText().toString();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getText().toString().equals("8888") && password.getText().toString().equals("admin")) {
                    startActivity(intent);
                } else {

                    Toast.makeText(LoginPage.this, "username 8888 dan password adalah admin", Toast.LENGTH_SHORT).show();
                }
            }
        });


        final Intent daftar = new Intent(this, SignUpPage.class);
        Button register = findViewById(R.id.btn_signup);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(daftar);
            }
        });


    }

}
