package com.timc.batch6.destar_desapintar.home.model;

import com.timc.batch6.destar_desapintar.home.GridHomeFragment;

public interface OnItemClickListener {

    void onItemClickListener(Item item, int position);
}
