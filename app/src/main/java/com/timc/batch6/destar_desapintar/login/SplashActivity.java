package com.timc.batch6.destar_desapintar.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.timc.batch6.destar_desapintar.MainActivity;
import com.timc.batch6.destar_desapintar.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 4/20/18.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getSupportActionBar().hide();

        delay();
    }

    // Membuat flash opening selama n detik kemudian menjalankan activity utama
    private void delay() {

        Timer t = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        };
        t.schedule(timerTask, 3000);
        ;
    }
}
