package com.timc.batch6.destar_desapintar.account;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.login.LoginPage;

public class MyProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        final TextView textView = findViewById(R.id.tv_Keluar);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfile.this, LoginPage.class));
            }
        });

    }
}
