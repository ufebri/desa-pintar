package com.timc.batch6.destar_desapintar.home.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.home.model.Item;

import java.util.List;

public class HomeCardAdapter extends RecyclerView.Adapter<HomeCardAdapter.Holder> {

    final private List<Item> itemsList ;

    public HomeCardAdapter(List<Item> itemsList) {
        this.itemsList = itemsList;
    }

    @Override
    public HomeCardAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_home, parent, false);
        return new  Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 20;
    }


    class Holder extends RecyclerView.ViewHolder{
        public Holder(View itemView) {
            super(itemView);
        }
    }
}
