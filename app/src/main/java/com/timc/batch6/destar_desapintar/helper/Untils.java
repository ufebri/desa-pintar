package com.timc.batch6.destar_desapintar.helper;

import android.content.Context;
import android.net.ConnectivityManager;


/**
 * Created by Owl on 23/02/2018.
 */

public class Untils {
    private Context context;
    private String priceSub;

    public String getPriceSub() {
        priceSub = priceSub.replace(".0","");
        if (priceSub.length() == 4) {
            String sub1 = priceSub.substring(0, 1);
            String sub2 = priceSub.substring(1);
            priceSub = "Rp. " + sub1 + "." + sub2;
        } else if (priceSub.length() == 5) {
            String sub1 = priceSub.substring(0, 2);
            String sub2 = priceSub.substring(2);
            priceSub = "Rp. " + sub1 + "." + sub2;
        } else if (priceSub.length() == 6) {
            String sub1 = priceSub.substring(0, 3);
            String sub2 = priceSub.substring(3);
            priceSub = "Rp. " + sub1 + "." + sub2;
        } else if (priceSub.length() == 7) {
            String sub1 = priceSub.substring(0, 1);
            String sub2 = priceSub.substring(1, 4);
            String sub3 = priceSub.substring(4);
            priceSub = "Rp. " + sub1 + "." + sub2 + "." + sub3;
        } else if (priceSub.length() == 8) {
            String sub1 = priceSub.substring(0, 2);
            String sub2 = priceSub.substring(2, 5);
            String sub3 = priceSub.substring(5);
            priceSub = "Rp. " + sub1 + "." + sub2 + "." + sub3;
        } else if (priceSub.length() == 9) {
            String sub1 = priceSub.substring(0, 3);
            String sub2 = priceSub.substring(3, 6);
            String sub3 = priceSub.substring(6);
            priceSub = "Rp. " + sub1 + "." + sub2 + "." + sub3;
        } else if (priceSub.length() == 10) {
            String sub1 = priceSub.substring(0, 1);
            String sub2 = priceSub.substring(1, 4);
            String sub3 = priceSub.substring(4, 7);
            String sub4 = priceSub.substring(7);
            priceSub = "Rp. " + sub1 + "." + sub2 + "." + sub3 + "." + sub4;
        } else {
            priceSub = "Rp. " + priceSub;
        }
        return priceSub;
    }

    public void setPriceSub(String priceSub) {
        this.priceSub = priceSub;
    }

    public Untils(Context context) {
        this.context = context;
    }

}
