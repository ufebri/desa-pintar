package com.timc.batch6.destar_desapintar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 4/20/18.
 */

public class LCategory {


    @SerializedName("categorys")
    @Expose
    private List<CategoryProduct> categorys = null;

    public List<CategoryProduct> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<CategoryProduct> categorys) {
        this.categorys = categorys;
    }

}
