package com.timc.batch6.destar_desapintar.myads.tab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timc.batch6.destar_desapintar.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NonActiveAdsFragment extends Fragment {


    public NonActiveAdsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_non_active, container, false);
    }

}
