package com.timc.batch6.destar_desapintar.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.timc.batch6.destar_desapintar.R;

/**
 * Created by user on 3/31/18.
 */

public class FilterPage extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        Spinner dropdownDesa = findViewById(R.id.spn_village);
        String[] itemsDesa = new String[]{"Desa", "Desa1", "Desa2", "Desa3"};
        ArrayAdapter<String> adapterDesa = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsDesa);
        dropdownDesa.setAdapter(adapterDesa);

        Spinner dropdownKecamatan = findViewById(R.id.spn_category);
        String[] itemsKecamatan = new String[]{"Kategori", "Produk Jadi", "Jasa", "Lowongan"};
        ArrayAdapter<String> adapterKecamatan = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsKecamatan);
        dropdownKecamatan.setAdapter(adapterKecamatan);

        //add icon back to activity
        ActionBar menu = getSupportActionBar();
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setDisplayShowHomeEnabled(true);

    }




    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
