package com.timc.batch6.destar_desapintar.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/20/18.
 */

public class PostProduct {
    @SerializedName("name")
    private String name;

    @SerializedName("category_id")
    private int categoryId;

    @SerializedName("price")
    private int price;

    public PostProduct(String name, int categoryId, int price) {
        this.name = name;
        this.categoryId = categoryId;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
