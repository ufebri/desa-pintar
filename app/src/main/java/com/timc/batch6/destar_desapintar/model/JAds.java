package com.timc.batch6.destar_desapintar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 4/16/18.
 */

public class JAds {
    @SerializedName("categorys")
    @Expose
    private List<CategoryProduct> ads;

    public List<CategoryProduct> getAds() {
        return ads;
    }

    public void setAds(List<CategoryProduct> ads) {
        this.ads = ads;
    }
}
