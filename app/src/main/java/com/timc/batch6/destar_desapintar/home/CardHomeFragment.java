package com.timc.batch6.destar_desapintar.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.content.DetailContentActivity;
import com.timc.batch6.destar_desapintar.home.adapter.HomeCardAdapter;
import com.timc.batch6.destar_desapintar.home.model.Item;
import com.timc.batch6.destar_desapintar.home.model.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class CardHomeFragment extends Fragment implements OnItemClickListener {

    private List<Item> itemsList = new ArrayList<>();
    private HomeCardAdapter adapter;

    public CardHomeFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_home, container, false);
        adapter = new HomeCardAdapter(itemsList);
        RecyclerView recyclerView = view.findViewById(R.id.recCarddHome);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);

        getItem();

        return view;
    }

    private void getItem(){
        itemsList.add(new Item("Telur Asin", "Rp. 10000", R.drawable.telor_asin));
        itemsList.add(new Item("Sayuran Segar", "Rp. 5000", R.drawable.sayuran));
        itemsList.add(new Item("Emping Renyah", "Rp. 7000", R.drawable.emping));
        itemsList.add(new Item("Gerabah Berkualitas", "Rp. 70000", R.drawable.gerabah));
        itemsList.add(new Item("Tomat Segar", "Rp. 8000", R.drawable.tomat));
        itemsList.add(new Item("Baju Wanita", "Rp. 40000", R.drawable.baju));
        itemsList.add(new Item("Tomat Segar", "Rp. 30000", R.drawable.tomat));
        itemsList.add(new Item("Gerabah Berkualitas", "Rp. 70000", R.drawable.gerabah));
        itemsList.add(new Item("Ubi Ungun", "Rp. 150000", R.drawable.ubi));
        itemsList.add(new Item("Tomat Segar", "Rp. 10000", R.drawable.tomat));
        itemsList.add(new Item("Cabe Merah Segar", "Rp. 10000", R.drawable.cabe_merah));
        itemsList.add(new Item("Kambing", "Rp. 4000000", R.drawable.kambing));
    }

    @Override
    public void onItemClickListener(Item item, int position) {
        Intent intent = new Intent(getActivity(), DetailContentActivity.class); //get context
        intent.putExtra("item", item);
        startActivity(intent);
    }
}
