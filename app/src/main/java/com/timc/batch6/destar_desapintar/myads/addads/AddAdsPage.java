package com.timc.batch6.destar_desapintar.myads.addads;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.helper.DBHandler;
import com.timc.batch6.destar_desapintar.model.Ads;
import com.timc.batch6.destar_desapintar.model.CategoryProduct;
import com.timc.batch6.destar_desapintar.model.IDVillage;
import com.timc.batch6.destar_desapintar.model.JAds;
import com.timc.batch6.destar_desapintar.model.JProductsRes;
import com.timc.batch6.destar_desapintar.model.JProdutcs;
import com.timc.batch6.destar_desapintar.model.LCategory;
import com.timc.batch6.destar_desapintar.model.PostProduct;
import com.timc.batch6.destar_desapintar.myads.network.ApiService;
import com.timc.batch6.destar_desapintar.myads.network.BaseApp;
import com.timc.batch6.destar_desapintar.myads.tab.ActiveAdsFragment;
import com.timc.batch6.destar_desapintar.util.UtilsApi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asinalopes on 24/03/18.
 */


public class AddAdsPage extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;
    List<CategoryProduct> categoryProducts = new ArrayList<>();

    ImageView viewImage;
    Button b;

    @BindView(R.id.spn_desa)
    Spinner pilihDesa;
    @BindView(R.id.edt_adsTitle)
    EditText namaProduk;
    @BindView(R.id.edt_price)
    EditText hargaProduk;
    @BindView(R.id.spn_category)
    Spinner kategoriProduk;
    @BindView(R.id.edt_adsDes)
    EditText tulisDeskripsi;

    ProgressDialog loading;

    ApiService mApiService;
    Context mContext;
    String token;

    private String[] itemsCategoty = new String[]{"Produk Jadi"};
    private DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_page);

        token = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1MjY5MTc1ODJ9.4cN-NxkqsX495-xy13e04HquZ9bEUFY1fg8IBQhRRQ4";

        dbHandler = new DBHandler(AddAdsPage.this);
        initComponents();
        spinnerDesa();

        ButterKnife.bind(this);
        mContext = this;
        mApiService = UtilsApi.getAPIService();

        Button btnTambahProduk = findViewById(R.id.btn_addAds);
        btnTambahProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSimpanProduk();
            }
        });

        initSpinner();

        kategoriProduk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedname = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Aktivitas untuk menambah gambar

        b = findViewById(R.id.btn_addImg);

        viewImage = findViewById(R.id.iv_adsImg);

        b.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                //  selectImage();
                if (Build.VERSION.SDK_INT >= 21) {
                    if (ContextCompat.checkSelfPermission(AddAdsPage.this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(AddAdsPage.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(AddAdsPage.this,

                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        List<String> permision = new ArrayList<String>();
                        permision.add(Manifest.permission.CAMERA);
                        permision.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        permision.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        ActivityCompat.requestPermissions(AddAdsPage.this,
                                permision.toArray(new String[permision.size()]),
                                REQUEST_CODE);

                    } else {
                        selectImage();
                    }


                } else {
                    selectImage();
                }
            }
        });


    }

    private void spinnerDesa() {
        loading = ProgressDialog.show(AddAdsPage.this, null, "harap tunggu...", true, false);

        Call<List<IDVillage>> adsCall = BaseApp.service.getVillage();
        adsCall.enqueue(new Callback<List<IDVillage>>() {
            @Override
            public void onResponse(Call<List<IDVillage>> call, Response<List<IDVillage>> response) {
                if (response.isSuccessful()) {
                    loading.dismiss();
                    List<IDVillage> categoryProducts = response.body();
                    List<String> listSpinner = new ArrayList<>();
                    for (int i = 0; i < categoryProducts.size(); i++) {
                        listSpinner.add(categoryProducts.get(i).getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                            R.layout.support_simple_spinner_dropdown_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    pilihDesa.setAdapter(adapter);
                } else {
                    loading.dismiss();
                    Toast.makeText(mContext, "Gagal mengambil data kategori", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<IDVillage>> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initSpinner() {
//

//        BaseApp.service.getCategory().enqueue(new Callback<CategoryProduct>() {
//            @Override
//            public void onResponse(Call<CategoryProduct> call, Response<CategoryProduct> response) {
//                if (response.isSuccessful()) {
//                    loading.dismiss();
//                    List<CategoryProduct> categoryProducts = response.body()
//                    List<String> listSpinner = new ArrayList<String>();
//                    for (int i = 0; i < categoryProducts.size(); i++) {
//                        listSpinner.add(categoryProducts.get(i).getName());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
//                            R.layout.support_simple_spinner_dropdown_item, listSpinner);
//                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//                    kategoriProduk.setAdapter(adapter);
//                } else {
//                    loading.dismiss();
//                    Toast.makeText(mContext, "Gagal mengambil data kategori", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CategoryProduct> call, Throwable t) {
//                loading.dismiss();
//                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
//            }
//        });

//        Call<JAds> adsCall = BaseApp.service.getCategory();
//        adsCall.enqueue(new Callback<JAds>() {
//            @Override
//            public void onResponse(Call<JAds> call, Response<JAds> response) {
//                if (response.isSuccessful()) {
//                    loading.dismiss();
//                    List<CategoryProduct> categoryProducts = response.body().getAds();
        List<String> listSpinner = new ArrayList<>();
//                    for (int i = 0; i < categoryProducts.size(); i++) {
//                        listSpinner.add(categoryProducts.get(i).getName());
//                    }
        listSpinner.add("product");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                R.layout.support_simple_spinner_dropdown_item, listSpinner);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        kategoriProduk.setAdapter(adapter);
//                } else {
//                    loading.dismiss();
//                    Toast.makeText(mContext, "Gagal mengambil data kategori", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JAds> call, Throwable t) {
//                loading.dismiss();
//                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    private void initComponents() {
        Button Tambahiklan = findViewById(R.id.btn_addAds);
        Tambahiklan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddAdsPage.this, ActiveAdsFragment.class));
            }
        });
    }

    private void requestSimpanProduk() {

        loading = ProgressDialog.show(mContext, null, "Sedang Memuat . . .",
                true, false);

        JProdutcs produtcs = new JProdutcs();
        produtcs.setName(namaProduk.getText().toString());
        produtcs.setVillageId(pilihDesa.getSelectedItemPosition());
        produtcs.setProductType(kategoriProduk.getSelectedItem().toString());
        produtcs.setHighestPrice(Integer.parseInt(hargaProduk.getText().toString()));
        produtcs.setDescription(tulisDeskripsi.getText().toString());
        produtcs.setUserId("2");


        BaseApp.service.postProducts(token, produtcs).enqueue(new Callback<JProductsRes>() {
            @Override
            public void onResponse(Call<JProductsRes> call, Response<JProductsRes> response) {
                if (response.isSuccessful()) {
                    loading.dismiss();
                    Toast.makeText(mContext, "Gagal di tambahkan", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    loading.dismiss();
                    Toast.makeText(mContext, "Produk Berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JProductsRes> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Jaringan Anda bermasalah", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int access = 0;
        for (int i = 0; i < grantResults.length; i++) {
            access++;
        }

        if (requestCode == REQUEST_CODE) {
            selectImage();
        }
    }

    private void selectImage() {


        final CharSequence[] options = {"Ambil Gambar", "Pilih dari Gallery", "Batalkan"};


        AlertDialog.Builder builder = new AlertDialog.Builder(AddAdsPage.this);

        builder.setTitle("Tambahkan Gambar!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals(options[0]))

                {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);

                } else if (options[item].equals(options[1]))

                {

                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);


                } else if (options[item].equals(options[2])) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                File f = new File(Environment.getExternalStorageDirectory().toString());

                for (File temp : f.listFiles()) {

                    if (temp.getName().equals("temp.jpg")) {

                        f = temp;

                        break;

                    }

                }

                try {

                    Bitmap bitmap;

                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();


                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),

                            bitmapOptions);


                    viewImage.setImageBitmap(bitmap);


                    String path = android.os.Environment

                            .getExternalStorageDirectory()

                            + File.separator

                            + "Phoenix" + File.separator + "default";

                    f.delete();

                    OutputStream outFile = null;

                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {

                        outFile = new FileOutputStream(file);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);

                        outFile.flush();

                        outFile.close();

                    } catch (FileNotFoundException e) {

                        e.printStackTrace();

                    } catch (IOException e) {

                        e.printStackTrace();

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            } else if (requestCode == 2) {


                Uri selectedImage = data.getData();

                String[] filePath = {MediaStore.Images.Media.DATA};

                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                c.close();

                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

                Log.w("Lopes", picturePath + "");

                viewImage.setImageBitmap(thumbnail);

            }

        }


    }

}