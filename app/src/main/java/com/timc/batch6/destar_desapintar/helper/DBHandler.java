package com.timc.batch6.destar_desapintar.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.timc.batch6.destar_desapintar.model.AdsLocal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/19/18.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "db_iklan";
    private static final String TABLE_IKLAN = "table_iklan";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_JUDUL = "judul";
    private static final String COLUMN_DESA = "desa";
    private static final String COLUMN_KATEGORI = "kategori";
    private static final String COLUMN_HARGA = "harga";
    private static final String COLUMN_DESKRIPSI = "deskripsi";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Fungsi buat Database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE" + TABLE_IKLAN + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_JUDUL +
                " TEXT," + COLUMN_DESA + " TEXT," + COLUMN_KATEGORI +
                " TEXT," + COLUMN_HARGA + " TEXT," + COLUMN_DESKRIPSI +
                " TEXT" + ")";
        db.execSQL(CREATE_USER_TABLE);

    }

    //Fungsi memastikan ada Database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IKLAN);
        onCreate(db);

    }

    //Fungsi Untuk Tambah iklan
    public void tambahIklan(AdsLocal iklan){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_JUDUL, iklan.getJudulIklan());
        values.put(COLUMN_DESA, iklan.getPilihDesa());
        values.put(COLUMN_KATEGORI, iklan.getPilihKategori());
        values.put(COLUMN_HARGA, iklan.getHargaIklan());
        values.put(COLUMN_DESKRIPSI, iklan.getDeskripsiIklan());

        db.insert(TABLE_IKLAN, null, values);
        db.close();

    }

    public List<AdsLocal> getSemuaIklan(){
        List<AdsLocal> adsLocalList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_IKLAN;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToNext()){
            do {
                AdsLocal adsLocal = new AdsLocal(cursor.getString(1), cursor.getString(2));
                adsLocalList.add(adsLocal);
            } while (cursor.moveToNext());
        }
        return adsLocalList;
    }


//    Cursor cursor = db.query(TABLE_IKLAN, new String[]{COLUMN_ID, COLUMN_JUDUL,
//            COLUMN_DESA, COLUMN_KATEGORI, COLUMN_HARGA, COLUMN_DESKRIPSI}, COLUMN_ID + "=?", new String[]
//            {String.valueOf(id_iklan)}, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
}
