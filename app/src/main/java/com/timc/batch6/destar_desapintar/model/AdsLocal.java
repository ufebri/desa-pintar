package com.timc.batch6.destar_desapintar.model;

/**
 * Created by user on 4/19/18.
 */

public class AdsLocal {
    private int idIklan, gambarIklan, hargaIklan;
    private String judulIklan, pilihDesa, pilihKategori, deskripsiIklan;


    public AdsLocal(String judulIklan, String pilihDesa) {
        this.idIklan = idIklan;
        this.gambarIklan = gambarIklan;
        this.hargaIklan = hargaIklan;
        this.judulIklan = judulIklan;
        this.pilihDesa = pilihDesa;
        this.pilihKategori = pilihKategori;
        this.deskripsiIklan = deskripsiIklan;
    }

    public int getIdIklan() {
        return idIklan;
    }

    public void setIdIklan(int idIklan) {
        this.idIklan = idIklan;
    }

    public int getGambarIklan() {
        return gambarIklan;
    }

    public void setGambarIklan(int gambarIklan) {
        this.gambarIklan = gambarIklan;
    }

    public int getHargaIklan() {
        return hargaIklan;
    }

    public void setHargaIklan(int hargaIklan) {
        this.hargaIklan = hargaIklan;
    }

    public String getJudulIklan() {
        return judulIklan;
    }

    public void setJudulIklan(String judulIklan) {
        this.judulIklan = judulIklan;
    }

    public String getPilihDesa() {
        return pilihDesa;
    }

    public void setPilihDesa(String pilihDesa) {
        this.pilihDesa = pilihDesa;
    }

    public String getPilihKategori() {
        return pilihKategori;
    }

    public void setPilihKategori(String pilihKategori) {
        this.pilihKategori = pilihKategori;
    }

    public String getDeskripsiIklan() {
        return deskripsiIklan;
    }

    public void setDeskripsiIklan(String deskripsiIklan) {
        this.deskripsiIklan = deskripsiIklan;
    }
}
