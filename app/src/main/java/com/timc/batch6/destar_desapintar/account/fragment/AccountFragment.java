package com.timc.batch6.destar_desapintar.account.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.account.AboutPage;
import com.timc.batch6.destar_desapintar.account.HelpPage;
import com.timc.batch6.destar_desapintar.account.InfoPage;
import com.timc.batch6.destar_desapintar.account.MyProfile;

/**
 * Created by djodhy on 24/03/18.
 */


public class AccountFragment extends Fragment {

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_account, container, false);

       //Klik ke activity profil
        LinearLayout profil = rootView.findViewById(R.id.ll_profil);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickProfil();
            }
        });

        return rootView;



    }

    //Klik ke Activity Profil
    public void clickProfil() {
        Intent intent = new Intent(getActivity(), MyProfile.class);
        startActivity(intent);
    }


}

