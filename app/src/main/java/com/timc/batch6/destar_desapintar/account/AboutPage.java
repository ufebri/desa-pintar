package com.timc.batch6.destar_desapintar.account;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.timc.batch6.destar_desapintar.R;

public class AboutPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_page);
    }
}
