package com.timc.batch6.destar_desapintar.notifikasi;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.timc.batch6.destar_desapintar.R;

public class NotificationFragment extends Fragment {

    public static NotificationFragment newInstance(){
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    public NotificationFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        Button btnterima = view.findViewById(R.id.btn_terima);
        btnterima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ButtonProsesFragment NAME = new ButtonProsesFragment();
                fragmentTransaction.replace(R.id.frame_container, NAME);
                fragmentTransaction.commit();
            }
        });

        return (view);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}

