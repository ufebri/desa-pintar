package com.timc.batch6.destar_desapintar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.home.HomeFragment;
import com.timc.batch6.destar_desapintar.account.fragment.AccountFragment;
import com.timc.batch6.destar_desapintar.home.fragment.MyAdsFragment;
import com.timc.batch6.destar_desapintar.notifikasi.NotificationFragment;
import com.timc.batch6.destar_desapintar.helper.BottomNavigationBar;

/**
 * Created by user on 3/29/18.
 */


public class MainActivity extends AppCompatActivity {

   private TextView Search_Result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        commitFragment(HomeFragment.newInstance());
        //id buat query pencarian
        Search_Result = findViewById(R.id.tv_search_result);

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        BottomNavigationBar.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                selectedFragment = HomeFragment.newInstance();
                                getSupportActionBar().setTitle("Destar");
                                break;
                            case R.id.action_myads:
                                selectedFragment = MyAdsFragment.newInstance();
                                getSupportActionBar().setTitle("Iklan Saya");
                                break;
                            case R.id.action_notifikasi:
                                selectedFragment = NotificationFragment.newInstance();
                                getSupportActionBar().setTitle("Notifikasi");
                                break;
                            case R.id.action_profile:
                                selectedFragment = AccountFragment.newInstance();
                                getSupportActionBar().setTitle("Akun");
                        }
                        commitFragment(selectedFragment);
                        return true;
                    }
                });
    }

    private void commitFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
//        inflater.inflate(R.menu.menu_filter, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // lakukan query disini
                Search_Result.setText("Hasil pencarian :" + query);

                searchView.clearFocus();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


}

