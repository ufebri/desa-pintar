package com.timc.batch6.destar_desapintar.content;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.timc.batch6.destar_desapintar.MainActivity;
import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.home.HomeFragment;


public class BarterPage extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barter_page);

        getSupportActionBar().setTitle("Barter");

        Button btnKirim = findViewById(R.id.btn_kirim);
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(BarterPage.this, MainActivity.class);
                startActivity(intent);

                Toast.makeText(BarterPage.this, "Data Sedang di Proses", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
