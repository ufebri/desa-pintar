package com.timc.batch6.destar_desapintar.myads.network;

import com.timc.batch6.destar_desapintar.model.Ads;
import com.timc.batch6.destar_desapintar.model.CategoryProduct;
import com.timc.batch6.destar_desapintar.model.IDVillage;
import com.timc.batch6.destar_desapintar.model.JAds;
import com.timc.batch6.destar_desapintar.model.JProductsRes;
import com.timc.batch6.destar_desapintar.model.JProdutcs;
import com.timc.batch6.destar_desapintar.model.NotifBarter;
import com.timc.batch6.destar_desapintar.model.PostProduct;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by user on 4/15/18.
 */

public interface ApiService {
    @GET("v1/products")
    Call<List<Ads>> getProducts();

    @POST("v1/products")
    Call<List<Ads>> saveAds(@Body PostProduct product);

    @GET("v1/products")
    Call<JProductsRes> getCategory();

    @GET("v1/villages")
    Call<List<IDVillage>> getVillage();

    @GET("v1/barters")
    Call<NotifBarter> getNotif();

    // post product
    @POST("v1/products")
    Call<JProductsRes> postProducts(@Header("Authorization") String token, @Body JProdutcs produtcs);
}
