package com.timc.batch6.destar_desapintar.util;

import com.timc.batch6.destar_desapintar.myads.network.ApiService;

/**
 * Created by user on 4/18/18.
 */

public class UtilsApi {

    public static final String BASE_URL_API = "https://destar.herokuapp.com/";

    public static ApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(ApiService.class);
    }

}
