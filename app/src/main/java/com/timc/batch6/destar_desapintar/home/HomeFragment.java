package com.timc.batch6.destar_desapintar.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;

public class HomeFragment extends Fragment {

    private SectionsPageAdapter pageAdapter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        TextView pilihDesa = v.findViewById(R.id.tv_location);
        pilihDesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent klikDesa = new Intent(getActivity(), SelectVillage.class);
                startActivity(klikDesa);
            }
        });

        TextView pilihFilter = v.findViewById(R.id.tv_filter);
        pilihFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent klikFilter = new Intent(getActivity(), FilterPage.class);
                startActivity(klikFilter);
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialized(view);
    }

    private void initialized(View view){
        final TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        final ViewPager viewPager = view.findViewById(R.id.pager);
        pageAdapter = new SectionsPageAdapter(getChildFragmentManager());
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}

