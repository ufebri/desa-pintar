package com.timc.batch6.destar_desapintar.myads.contentAds;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.timc.batch6.destar_desapintar.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asinalopes on 13/04/18.
 */

public class EditAdsPage extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;

    ImageView viewImage;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ads);


        Spinner dropdownDesa = findViewById(R.id.spn_desa);
        String[] itemsDesa = new String[]{"Desa Kranggan", "Desa Kepurun", "Desa Tanjungsari", "Desa Kepurun"};
        ArrayAdapter<String> adapterDesa = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsDesa);
        dropdownDesa.setAdapter(adapterDesa);

        Spinner dropdownCategory = findViewById(R.id.spn_category);
        String[] itemsCategoty = new String[]{"Produk Jadi"};
        ArrayAdapter<String> adapterCategory = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsCategoty);
        dropdownCategory.setAdapter(adapterCategory);

        // Aktivitas untuk menambah gambar

        b = findViewById(R.id.btn_addImg);

        viewImage = findViewById(R.id.iv_adsImg);

        b.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                //  selectImage();
                if (Build.VERSION.SDK_INT >= 21) {
                    if (ContextCompat.checkSelfPermission(EditAdsPage.this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(EditAdsPage.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(EditAdsPage.this,

                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        List<String> permision = new ArrayList<String>();
                        permision.add(Manifest.permission.CAMERA);
                        permision.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        permision.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        ActivityCompat.requestPermissions(EditAdsPage.this,
                                permision.toArray(new String[permision.size()]),
                                REQUEST_CODE);

                    } else {
                        selectImage();
                    }


                } else {
                    selectImage();
                }
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int access = 0;
        for (int i = 0; i < grantResults.length; i++) {
            access++;
        }

        if (access >= 2) {
            selectImage();
        }
    }

    private void selectImage() {


        final CharSequence[] options = {"Ambil Gambar", "Pilih dari Gallery", "Batalkan"};


        AlertDialog.Builder builder = new AlertDialog.Builder(EditAdsPage.this);

        builder.setTitle("Tambahkan Gambar!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);

                } else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);


                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                File f = new File(Environment.getExternalStorageDirectory().toString());

                for (File temp : f.listFiles()) {

                    if (temp.getName().equals("temp.jpg")) {

                        f = temp;

                        break;

                    }

                }

                try {

                    Bitmap bitmap;

                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();


                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),

                            bitmapOptions);


                    viewImage.setImageBitmap(bitmap);


                    String path = android.os.Environment

                            .getExternalStorageDirectory()

                            + File.separator

                            + "Phoenix" + File.separator + "default";

                    f.delete();

                    OutputStream outFile = null;

                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {

                        outFile = new FileOutputStream(file);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);

                        outFile.flush();

                        outFile.close();

                    } catch (FileNotFoundException e) {

                        e.printStackTrace();

                    } catch (IOException e) {

                        e.printStackTrace();

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            } else if (requestCode == 2) {


                Uri selectedImage = data.getData();

                String[] filePath = {MediaStore.Images.Media.DATA};

                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                c.close();

                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

                Log.w("Lopes", picturePath + "");

                viewImage.setImageBitmap(thumbnail);

            }

        }


    }
}