package com.timc.batch6.destar_desapintar.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.timc.batch6.destar_desapintar.MainActivity;
import com.timc.batch6.destar_desapintar.R;

/**
 * Created by djodhy on 27/03/18.
 */

public class SignUpPage extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Menghilangkan ActionBar
        getSupportActionBar().hide();


        final Intent masuk = new Intent(this, LoginPage.class);
        Button login = findViewById(R.id.btn_Login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(masuk);
            }
        });

        final Intent daftar = new Intent(this, MainActivity.class);
        Button register = findViewById(R.id.btn_Register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(daftar);
            }
        });


    }


}
