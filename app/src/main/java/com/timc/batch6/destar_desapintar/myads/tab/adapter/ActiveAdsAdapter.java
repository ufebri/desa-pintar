package com.timc.batch6.destar_desapintar.myads.tab.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.Until;
import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.helper.Untils;
import com.timc.batch6.destar_desapintar.model.AdsLocal;
import com.timc.batch6.destar_desapintar.myads.addads.AddAdsPage;
import com.timc.batch6.destar_desapintar.model.Ads;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/15/18.
 */

public class ActiveAdsAdapter extends RecyclerView.Adapter<ActiveAdsAdapter.Holder> {

    private List<Ads> adsGrid;
    Context context;

    public ActiveAdsAdapter(List<Ads> adsGrid) {
        this.adsGrid = adsGrid;
    }

    @Override
    public ActiveAdsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads,
                parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final ActiveAdsAdapter.Holder holder, final int position) {
        holder.bind(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogComment(holder.getAdapterPosition());
            }
        });

    }

    private void createDialogComment(final int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);;
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_delete_item, null);
        final TextView edit = view.findViewById(R.id.tv_Edit);
        final TextView delete = view.findViewById(R.id.tv_hapus);
        final Intent intent = new Intent(context, AddAdsPage.class);

         builder.setView(view);
         builder.show();

         edit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 context.startActivity(intent);
             }
         });

         delete.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 adsGrid.remove(position);
                 notifyDataSetChanged();
             }
         });


    } // dialog

    @Override
    public int getItemCount() {
        return adsGrid.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);


        }

        public void bind(int position) {
            Ads iklan = adsGrid.get(position);
            ImageView gambarIklan = itemView.findViewById(R.id.iv_iklan);
            TextView namaIklan = itemView.findViewById(R.id.tv_nama_iklan);
            TextView hargaIklan = itemView.findViewById(R.id.tv_harga_iklan);

            Glide.with(context)
                    .load(iklan.getPictureList().getUrl())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(gambarIklan);
            namaIklan.setText(iklan.getName());
            Untils untils = new Untils(context);
            if(iklan.getPrice() != null){
                untils.setPriceSub(iklan.getPrice().toString());
                hargaIklan.setText(untils.getPriceSub());
            }

        }
    }
}
