package com.timc.batch6.destar_desapintar.content;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.content.slider.FragmentSlider;
import com.timc.batch6.destar_desapintar.content.slider.SliderIndicator;
import com.timc.batch6.destar_desapintar.content.slider.SliderPagerAdapter;
import com.timc.batch6.destar_desapintar.content.slider.SliderView;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;


public class DetailContentActivity extends AppCompatActivity {

    private static final String TAG = "ExpandableTextView";
    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;

    private SliderView sliderView;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_content);
        sliderView = findViewById(R.id.sliderView);
        mLinearLayout = findViewById(R.id.pagesContainer);
        setupSlider();

        Button btnBarter = findViewById(R.id.btn_barter);
        btnBarter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailContentActivity.this, BarterPage.class);
                startActivity(intent);
            }
        });

        final ExpandableTextView expandableTextView = this.findViewById(R.id.expandableTextView);
        final TextView readMore = this.findViewById(R.id.tv_selengkapnya);

        // set interpolators for both expanding and collapsing animations
        expandableTextView.setInterpolator(new OvershootInterpolator());

        // or set them separately
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());


        // but, you can also do the checks yourself
        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (expandableTextView.isExpanded()) {
                    expandableTextView.collapse();
                    readMore.setText(R.string.expand);
                } else {
                    expandableTextView.expand();
                    readMore.setText(R.string.collapse);
                }
            }
        });

        // listen for expand / collapse events
        expandableTextView.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(final ExpandableTextView view) {
                Log.d(TAG, "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(final ExpandableTextView view) {
                Log.d(TAG, "ExpandableTextView collapsed");
            }
        });

        getIncomingIntent();

    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("nama_Produk") && getIntent().hasExtra("harga_Produk")) {

            String judulProduk = getIntent().getStringExtra("nama_Produk");
            String hargaProduk = getIntent().getStringExtra("harga_Produk");

            setData(judulProduk, hargaProduk);
        }

    }

    private void setData(String judulProduk, String hargaProduk) {

        TextView judul = findViewById(R.id.tv_nama_produk);
        judul.setText(judulProduk);

        TextView harga = findViewById(R.id.tv_harga);
        harga.setText(hargaProduk);
    }

    private void setupSlider() {
        sliderView.setDurationScroll(800);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(FragmentSlider.newInstance("https://destar3.herokuapp.com//uploads/product/picture/17/picture.jpeg"));
        fragments.add(FragmentSlider.newInstance("https://destar3.herokuapp.com/uploads/product/picture/23/picture.jpeg"));
        fragments.add(FragmentSlider.newInstance("https://destar3.herokuapp.com//uploads/product/picture/16/picture.jpeg"));
        fragments.add(FragmentSlider.newInstance("https://destar3.herokuapp.com//uploads/product/picture/20/picture.jpeg"));
        fragments.add(FragmentSlider.newInstance("https://destar3.herokuapp.com//uploads/product/picture/15/picture.jpeg"));

        mAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(mAdapter);
        mIndicator = new SliderIndicator(this, mLinearLayout, sliderView, R.drawable.indicator_circle);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }

}
