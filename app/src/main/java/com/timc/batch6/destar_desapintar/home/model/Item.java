package com.timc.batch6.destar_desapintar.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Item implements Parcelable{

    private String title, price;
    private int image;

    public Item(){

    }

    public Item(String title, String  price, int image){
        this.title = title;
        this.price = price;
        this.image = image;
    }

    protected Item(Parcel in) {
        title = in.readString();
        price = in.readString();
        image = in.readInt();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getPrice() { return price; }

    public void setPrice(String  price) { this.price = price; }

    public int getImage() { return image; }

    public void setImage(int image) { this.image = image; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(price);
        parcel.writeInt(image);
    }

}
