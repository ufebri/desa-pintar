package com.timc.batch6.destar_desapintar.myads.tab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timc.batch6.destar_desapintar.R;
import com.timc.batch6.destar_desapintar.helper.DBHandler;
import com.timc.batch6.destar_desapintar.model.AdsLocal;
import com.timc.batch6.destar_desapintar.myads.network.BaseApp;
import com.timc.batch6.destar_desapintar.myads.tab.adapter.ActiveAdsAdapter;
import com.timc.batch6.destar_desapintar.model.Ads;
import com.timc.batch6.destar_desapintar.myads.tab.adapter.AdsLocalAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActiveAdsFragment extends Fragment {

    private String TAG = getClass().getSimpleName();
    ActiveAdsAdapter activeAdsAdapter;
    List<Ads> adsGrid = new ArrayList<>();

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;

    public ActiveAdsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_active_ads, container, false);

        RecyclerView rec_ads = view.findViewById(R.id.rec_iklan);
        rec_ads.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        activeAdsAdapter = new ActiveAdsAdapter(adsGrid);

        rec_ads.setAdapter(activeAdsAdapter);

        getProduct();

        initComponents(view);

        return view;
    }

    private void initComponents(View view) {
        recyclerView = view.findViewById(R.id.rec_iklan);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
    }


    private void getProduct() {
        BaseApp.service.getProducts().enqueue(new Callback<List<Ads>>() {
            @Override
            public void onResponse(Call<List<Ads>> call, Response<List<Ads>> response) {
                adsGrid.addAll(response.body());
                Log.d("Konek", response.toString() + response.body().size());
                adsGrid.size();
                activeAdsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Ads>> call, Throwable t) {
                Log.d("Gagal", t.getMessage());
            }
        });

    }


}
